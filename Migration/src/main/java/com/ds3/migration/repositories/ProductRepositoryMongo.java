package com.ds3.migration.repositories;

import com.ds3.migration.models.ProductEntityMongo;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ProductRepositoryMongo extends MongoRepository<ProductEntityMongo, String> {

}
