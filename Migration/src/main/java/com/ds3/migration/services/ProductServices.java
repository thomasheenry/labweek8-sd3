package com.ds3.migration.services;

import com.ds3.migration.models.DTOs.Request.ProductDTORequest;
import com.ds3.migration.models.ProductEntity;
import com.ds3.migration.repositories.ProductRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ProductServices {
    @Autowired
    private ProductRepository repository;


    public ProductEntity saveProduct(ProductDTORequest dto){
        ProductEntity product = new ProductEntity();
        BeanUtils.copyProperties(dto, product);
        return repository.save(product);
    }

    public List<ProductEntity> getAllProducts(){
        return repository.findAll();
    }

    public Optional<ProductEntity> getByID(UUID id){
        return repository.findById(id);
    }

    public Boolean deleteProduct(UUID id){
        Optional<ProductEntity> product = getByID(id);

        if (product.isEmpty()) return false;

        repository.delete(product.get());
        return true;
    }

    public ProductEntity updateProduct(ProductDTORequest dto, UUID id){
        Optional<ProductEntity> product = getByID(id);

        if (product.isEmpty()) return null;

        ProductEntity productEntity = product.get();
        BeanUtils.copyProperties(dto, productEntity);
        productEntity.setUpdatedAt(new Date());

        return repository.save(productEntity);
    }






}
