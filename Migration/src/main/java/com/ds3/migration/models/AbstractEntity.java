package com.ds3.migration.models;

import jakarta.persistence.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@MappedSuperclass
@Data
public abstract class AbstractEntity implements Serializable {

    AbstractEntity(){
        Date now = new Date();
        createdAt = now;
        updatedAt = now;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @Column(name = "DT_CREATED_AT")
    private final Date createdAt;

    @Column(name="DT_UPDATE_AT")
    private Date updatedAt;

}
