package com.ds3.migration.models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "TB_PRODUCTS")
@Data
public class ProductEntity extends AbstractEntity {

    @Column(name = "NM_PRODUCT", nullable = false)
    private String name;

    @Column(name = "VL_PRODUCT", nullable = false)
    private Double price;

    @Column(name= "STK_PRODUCTS", nullable = false)
    private Integer stock;
}
